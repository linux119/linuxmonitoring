netdevice=$(ip r | grep default | awk '/default/ {print $5}');
netmask=$(ifconfig "$netdevice" | awk '/netmask/{ print $4;}');
echo $netmask;