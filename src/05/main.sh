uptime_sec_start=$(cat /proc/uptime | awk -F'( )' '{print $1}');
last_symbol=$(echo $1 | rev | cut -c 1);
Incorrect=0;
if [ "$last_symbol" != "/" ]; then echo "Incorrect input. Last symbol should be '/'"; Incorrect=1; fi;
if ! [ -d "$1" ]; then echo 'No directory'; Incorrect=1; fi;

if (($Incorrect == 0)); then

#echo "Total number of folders (including all nested ones) = $(ls -laR "$1" 2>/dev/null | grep "^d" | wc | awk '{print $1}')"
$(du "$1" 2>/dev/null | sort -nr > buffer);
total_folders=$(($(cat buffer | wc -l) - 1));
echo "Total number of folders (including all nested ones) = $total_folders"
to=6;
if (($total_folders < 6)); then to=$total_folders; fi;
for (( i=2; i <= $to; i++ ))
do 
temp_str=$(cat buffer | awk "NR == $i");
echo $(($i - 1)) - $(echo "$temp_str" | sed 's|.*\t||')/, $(./size_print.sh $(echo "$temp_str" | awk '{print $1}') 0);
done
 $(find "$1" -type f -exec file {} \; 2>/dev/null > buffer);
num_of_files=$(cat buffer | wc -l);
echo Total number of files = $num_of_files;
if (($num_of_files > 0)); then
echo Number of:;
echo Configuration files \(with the .conf extension\) = $(cat buffer | grep ".*\.conf:.*" | wc -l);
echo Text files = $(cat buffer | grep ".*:.*text.*" | wc -l);
$(cat buffer | grep ".*:.*exe.*" > buffer3);
exe_files=$(cat buffer3 | wc -l);
echo Executable files = $exe_files;
echo Log files \(with the extension .log\) = $(cat buffer | grep ".*\.log:.*" | wc -l);
echo Archive files = $(cat buffer | grep ".*:.*compressed.*" | wc -l);
echo Symbolic links = $(find "$1" -type l 2>/dev/null | wc -l);
echo TOP 10 files of maximum size arranged in descending order \(path, size and type\):
$(find "$1" -type f -printf '%s %p\n' 2>/dev/null | sort -nr | head -10 > buffer2)
counter=1;
while read -r line
do
  name=$(echo $line | cut -c $(($(echo $line | awk '{print $1}' | wc -m)+1))-);
  ext_slash=$(echo $name | rev | awk -F"/" '{print $1}' | rev | wc -m);
  ext_dot_name=$(echo $name | rev | awk -F'[./]' '{print $1}' | rev);
  ext_dot=$(echo $ext_dot_name | wc -m);
  ext="(empty)";
  if (($ext_slash != $ext_dot)); then ext=$ext_dot_name; fi;
  echo $counter - $name, $(./size_print.sh $(echo $line | awk '{print $1}') 1), $ext;
  counter=$(($counter + 1));
done < buffer2
if (($exe_files > 0)); then
echo TOP 10 executable files of the maximum size arranged in descending order \(path, size and MD5 hash of file\);
while read -r line
do $(find "$(echo $line | awk -F'[:]' '{print $1}')" -printf '%s %p\n' >> buffer4);
done < buffer3
$(cat buffer4 | sort -nr | head -10 > buffer5)
counter=1;
while read -r line
do
  name=$(echo $line | cut -c $(($(echo $line | awk '{print $1}' | wc -m)+1))-);
  echo $counter - $name, $(./size_print.sh $(echo $line | awk '{print $1}') 1), $(md5sum "$name" | awk '{print $1}');
  counter=$(($counter + 1));
done < buffer5
else echo "No executable files";
fi;
fi;
fi;
rm -rf buffer buffer2 buffer3 buffer4 buffer5
uptime_sec_end=$(cat /proc/uptime | awk -F'( )' '{print $1}');
echo Script execution time \(in seconds\) = $(python -c "print('%.1f' %($uptime_sec_end - $uptime_sec_start))");