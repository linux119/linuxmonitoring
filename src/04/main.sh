str[0]=HOSTNAME;         res[0]=$HOSTNAME;
str[1]=TIMEZONE;         res[1]=$(./timezone.sh);
str[2]=USER;             res[2]=$USER;
str[3]=OS;               res[3]=$(./os.sh);
str[4]=DATE;             res[4]=$(date '+%d %b %Y %X');
str[5]=UPTIME;           res[5]=$(uptime | awk -F'( |,|:)' '{print $6,$7",",$9,"hours,",$10,"minutes."}');
str[6]=UPTIME_SEC;       res[6]=$(cat /proc/uptime | awk -F'( )' '{print $1}');
str[7]=IP;               res[7]=$(hostname -i);
str[8]=MASK;             res[8]=$(./netmask.sh);
str[9]=GATEWAY;          res[9]=$(ip r | grep default | awk '/default/ {print $3}');
str[10]=RAM_TOTAL;       res[10]=$(echo $(./ramtotal.sh total) GB);
str[11]=RAM_USED;        res[11]=$(echo $(./ramtotal.sh used) GB);
str[12]=RAM_FREE;        res[12]=$(echo $(./ramtotal.sh free) GB);
str[13]=SPACE_ROOT;      res[13]=$(echo $(./space_root.sh total) MB);
str[14]=SPACE_ROOT_USED; res[14]=$(echo $(./space_root.sh used) MB);
str[15]=SPACE_ROOT_FREE; res[15]=$(echo $(./space_root.sh free) MB);

colors=();
default=();
for i in 1 2 3 4
do colors[$i]=$(./conf_reader.sh $i); if [ "${colors[$i]}" == "" ]; then colors[$i]=$i; default[$i]=1; else default[$i]=0; fi;
done

incorrect=0;
for val in ${colors[*]}
do
    incorrect=$(($incorrect + $(./check_correct.sh $val)));
done
if (($incorrect == 0)); then
    if [ "${colors[1]}" == "${colors[2]}" ] && [ "${colors[1]}" != "-1" ]; then echo -e "Color of font and background of names equal\nTry again";
    elif [ "${colors[3]}" == "${colors[4]}" ] && [ "${colors[3]}" != "-1" ]; then echo -e "Color of font and background of values equal\nTry again"; 
    else 
        cbn=$(./colors_background.sh ${colors[1]});
        cfn=$(./colors_font.sh ${colors[2]});
        cbr=$(./colors_background.sh ${colors[3]});
        cfr=$(./colors_font.sh ${colors[4]});
        standart=$(./colors_background.sh 0);
        for index in ${!str[*]}
            do
                echo -ne "$cbn$cfn${str[$index]}$standart = $cfr$cbr${res[$index]}$standart\n";
            done
        echo
        for i in 1 2 3 4
        do ./print_conf.sh $i ${colors[$i]} ${default[$i]};
        done
    fi;
else
echo "Incorrect input"
fi