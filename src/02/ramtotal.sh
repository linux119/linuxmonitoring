memKb=0;
if [ "$1" == total ]; then memKb=$(free | grep Mem | awk '{print $2}');
elif [ "$1" == used ]; then memKb=$(free | grep Mem | awk '{print $3}');
elif [ "$1" == free ]; then memKb=$(free | grep Mem | awk '{print $4}'); fi;
res=$(python -c "print('%.3f' %($memKb / 1024.0 / 1024.0))");
echo $res;