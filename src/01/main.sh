res=$(echo $1 | grep -E "[[:alpha:]]|[[:blank:]]|[[:punct:]]");
if (($# == 1)) && [ -n "$res" ]; then
echo $1;
else
echo "Incorrect input"
fi