text=B
size=0;
if (($2 == 1)); then size=$1;
else size=$(($1 * 1024));
fi;
while (($(python -c "print('%.0f' %($size // 1024))") > 0))
do
    size=$(python -c "print('%.3f' %($size / 1024.0))");
    if [[ $text == B ]]; then text=KB;
    elif [[ $text == KB ]]; then text=MB;
    elif [[ $text == MB ]]; then text=GB;
    elif [[ $text == GB ]]; then text=TB;
    fi;
done
# size2=$(python -c "print('%.0f' %($size))");
# size=$(python -c "
# if $size > $size2: 
#     print('%.0f' %($size + 1)) 
# else: 
#     print('%.0f' %($size))");
echo $size $text