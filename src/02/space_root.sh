memKb=0;
if [ "$1" == total ]; then memKb=$(df -h -BK / | grep dev | awk '{print $2}' | rev | cut -c 2- | rev);
elif [ "$1" == used ]; then memKb=$(df -h -BK / | grep dev | awk '{print $3}' | rev | cut -c 2- | rev);
elif [ "$1" == free ]; then memKb=$(df -h -BK / | grep dev | awk '{print $4}' | rev | cut -c 2- | rev); fi;
res=$(python -c "print('%.2f' %($memKb / 1024.0))");
echo $res;