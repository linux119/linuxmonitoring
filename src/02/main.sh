host_name=$HOSTNAME;
time_zone=$(./timezone.sh);
user=$USER;
os=$(./os.sh);
date=$(date '+%d %b %Y %X');
uptime=$(uptime | awk -F'( |,|:)' '{print $6,$7",",$9,"hours,",$10,"minutes."}');
uptime_sec=$(cat /proc/uptime | awk -F'( )' '{print $1}');
ip=$(hostname -i);
netmask=$(./netmask.sh);
gateway=$(ip r | grep default | awk '/default/ {print $3}');
ram_total=$(./ramtotal.sh total);
ram_used=$(./ramtotal.sh used);
ram_free=$(./ramtotal.sh free);
space_root_total=$(./space_root.sh total);
space_root_used=$(./space_root.sh used);
space_root_free=$(./space_root.sh free);
res="HOSTNAME = $host_name\nTIMEZONE = $time_zone\nUSER = $user\nOS = $os\nDATE = $date\nUPTIME = $uptime\nUPTIME_SEC = $uptime_sec\nIP = $ip\nMASK = $netmask\nGATEWAY = $gateway\nRAM_TOTAL = $ram_total Gb\nRAM_USED = $ram_used Gb\nRAM_FREE = $ram_free Gb\nSPACE_ROOT = $space_root_total MB\nSPACE_ROOT_USED = $space_root_used MB\nSPACE_ROOT_FREE = $space_root_free MB";
echo -e $res"\n\nSave in file? Y/N: ";
read answer
if [ "$answer" == Y ] || [ "$answer" == y ]; then $(echo -e $res > $(date +'%d_%m_%y_%H_%M_%S').status); fi