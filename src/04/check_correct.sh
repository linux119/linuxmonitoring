res=$(echo $1 | grep -E "[[:alpha:]]|[[:blank:]]|[[:punct:]]");
if !([ -n "$res" ]) && (($1 >= 1)) && (($1 <= 6)) || (($1 == -1)) 2>/dev/null; then echo 0;
else echo 1;
fi;