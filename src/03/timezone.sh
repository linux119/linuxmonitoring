temp_str=$(echo `timedatectl | grep "Universal time"`);
#temp_end=$((${#temp_str} + 2));
#temp_start=$(($temp_end - 2));
temp_utc_gmt=$(echo $temp_str | rev | cut -c 1-3 | rev) #$(echo $temp_str | cut -c $temp_start-$temp_end);
temp_zone=$(cat /etc/timezone);
time_zone=$(echo $temp_zone $temp_utc_gmt `date +%Z`);
echo $time_zone