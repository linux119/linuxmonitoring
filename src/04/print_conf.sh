default="";
if (($1 == 1)); then
    if (($3 == 1)); then default="default"; else default=$2; fi;
    echo "Column 1 background = $default ($(./background_num_to_str.sh $2))";
elif (($1 == 2)); then
    if (($3 == 1)); then default="default"; else default=$2; fi;
    echo "Column 1 font color = $default ($(./background_num_to_str.sh $2))";
elif (($1 == 3)); then
    if (($3 == 1)); then default="default"; else default=$2; fi;
    echo "Column 2 background = $default ($(./background_num_to_str.sh $2))";
elif (($1 == 4)); then
    if (($3 == 1)); then default="default"; else default=$2; fi;
    echo "Column 2 font color = $default ($(./background_num_to_str.sh $2))";
 fi;